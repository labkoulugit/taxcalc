import { describe } from "mocha";
import { expect } from "chai";

import Tax from "../src/tax.mjs";

describe('Tax test suite', () => {
    it('can calculate price after tax', () => {
        const tax = 24;
        const list_price = 1;
        const expected_price_after_tax = 1.24;
        expect(Tax.applyTax(list_price, tax))
        .to
        .equal(expected_price_after_tax);
    });
});